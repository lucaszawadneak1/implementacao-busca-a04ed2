#include <iostream>

using namespace std;

#define RED true
#define BLACK false

struct Node
{
    int key;
    bool color;
    Node *left, *right, *parent;
};

void rotateLeft(Node *&root, Node *x)
{
    Node *y = x->right;
    x->right = y->left;
    if (y->left != NULL)
    {
        y->left->parent = x;
    }
    y->parent = x->parent;
    if (x->parent == NULL)
    {
        root = y;
    }
    else if (x == x->parent->left)
    {
        x->parent->left = y;
    }
    else
    {
        x->parent->right = y;
    }
    y->left = x;
    x->parent = y;
}

void rotateRight(Node *&root, Node *x)
{
    Node *y = x->left;
    x->left = y->right;
    if (y->right != NULL)
    {
        y->right->parent = x;
    }
    y->parent = x->parent;
    if (x->parent == NULL)
    {
        root = y;
    }
    else if (x == x->parent->right)
    {
        x->parent->right = y;
    }
    else
    {
        x->parent->left = y;
    }
    y->right = x;
    x->parent = y;
}

void fixInsert(Node *&root, Node *newNode)
{
    Node *parent = NULL;
    Node *grandParent = NULL;
    while (newNode != root && newNode->color != BLACK && newNode->parent->color == RED)
    {
        parent = newNode->parent;
        grandParent = parent->parent;
        if (parent == grandParent->left)
        {
            Node *uncle = grandParent->right;
            if (uncle != NULL && uncle->color == RED)
            {
                grandParent->color = RED;
                parent->color = BLACK;
                uncle->color = BLACK;
                newNode = grandParent;
            }
            else
            {
                if (newNode == parent->right)
                {
                    rotateLeft(root, parent);
                    newNode = parent;
                    parent = newNode->parent;
                }
                rotateRight(root, grandParent);
                std::swap(parent->color, grandParent->color);
                newNode = parent;
            }
        }
        else
        {
            Node *uncle = grandParent->left;
            if (uncle != NULL && uncle->color == RED)
            {
                grandParent->color = RED;
                parent->color = BLACK;
                uncle->color = BLACK;
                newNode = grandParent;
            }
            else
            {
                if (newNode == parent->left)
                {
                    rotateRight(root, parent);
                    newNode = parent;
                    parent = newNode->parent;
                }
                rotateLeft(root, grandParent);
                std::swap(parent->color, grandParent->color);
                newNode = parent;
            }
        }
    }
    root->color = BLACK;
}

// Função para inserir um novo nó na árvore
void insertNode(Node *&root, int key)
{
    // Cria um novo nó com a chave especificada
    Node *newNode = new Node();
    newNode->key = key;
    newNode->color = true;
    newNode->left = newNode->right = newNode->parent = NULL;

    // Caso a árvore esteja vazia, o novo nó é definido como a raiz
    if (root == NULL)
    {
        root = newNode;
        root->color = false;
        return;
    }

    // Encontra o lugar adequado para inserir o novo nó
    Node *current = root;
    Node *parent = NULL;
    while (current != NULL)
    {
        parent = current;
        if (key < current->key)
        {
            current = current->left;
        }
        else
        {
            current = current->right;
        }
    }

    // Insere o novo nó como filho do nó encontrado
    if (key < parent->key)
    {
        parent->left = newNode;
    }
    else
    {
        parent->right = newNode;
    }
    newNode->parent = parent;

    // Verifica e corrige as violações das propriedades da árvore Red-Black
    fixInsert(root, newNode);
}

void fixRemove(Node *&root, Node *x)
{
    Node *sibling = NULL;
    while (x != root && x->color == BLACK)
    {
        if (x == x->parent->left)
        {
            sibling = x->parent->right;
            if (sibling->color == RED)
            {
                sibling->color = BLACK;
                x->parent->color = RED;
                rotateLeft(root, x->parent);
                sibling = x->parent->right;
            }
            if (sibling->left->color == BLACK && sibling->right->color == BLACK)
            {
                sibling->color = RED;
                x = x->parent;
            }
            else
            {
                if (sibling->right->color == BLACK)
                {
                    sibling->left->color = BLACK;
                    sibling->color = RED;
                    rotateRight(root, sibling);
                    sibling = x->parent->right;
                }
                sibling->color = x->parent->color;
                x->parent->color = BLACK;
                sibling->right->color = BLACK;
                rotateLeft(root, x->parent);
                x = root;
            }
        }
        else
        {
            sibling = x->parent->left;
            if (sibling->color == RED)
            {
                sibling->color = BLACK;
                x->parent->color = RED;
                rotateRight(root, x->parent);
                sibling = x->parent->left;
            }
            if (sibling->right->color == BLACK && sibling->left->color == BLACK)
            {
                sibling->color = RED;
                x = x->parent;
            }
            else
            {
                if (sibling->left->color == BLACK)
                {
                    sibling->right->color = BLACK;
                    sibling->color = RED;
                    rotateLeft(root, sibling);
                    sibling = x->parent->left;
                }
                sibling->color = x->parent->color;
                x->parent->color = BLACK;
                sibling->left->color = BLACK;
                rotateRight(root, x->parent);
                x = root;
            }
        }
    }
    x->color = BLACK;
}

Node *findNode(Node *root, int key)
{
    while (root != NULL && root->key != key)
    {
        if (root->key < key)
        {
            root = root->right;
        }
        else
        {
            root = root->left;
        }
    }
    return root;
}

Node *getSuccessor(Node *root)
{
    root = root->right;
    while (root->left != NULL)
    {
        root = root->left;
    }
    return root;
}

// Função para remover um nó da árvore
void removeNode(Node *&root, int key)
{
    Node *toRemove = findNode(root, key);
    if (toRemove == NULL)
    {
        return;
    }

    Node *replacement;
    if (toRemove->left == NULL || toRemove->right == NULL)
    {
        replacement = toRemove;
    }
    else
    {
        replacement = getSuccessor(toRemove);
    }

    Node *child = (replacement->left != NULL) ? replacement->left : replacement->right;
    if (child != NULL)
    {
        child->parent = replacement->parent;
    }

    if (replacement->parent == NULL)
    {
        root = child;
    }
    else if (replacement == replacement->parent->left)
    {
        replacement->parent->left = child;
    }
    else
    {
        replacement->parent->right = child;
    }

    if (replacement != toRemove)
    {
        toRemove->key = replacement->key;
    }

    if (!replacement->color)
    {
        fixRemove(root, child);
    }

    delete replacement;
}

void printTree(Node *root)
{
    if (root == NULL)
        return;
    cout << root->key << " ";
    printTree(root->left);
    printTree(root->right);
}

int main()
{
    Node *root = NULL;

    printTree(root);
    cout << endl;

    insertNode(root, 7);
    insertNode(root, 3);
    insertNode(root, 18);
    insertNode(root, 10);
    insertNode(root, 22);

    printTree(root);
    cout << endl;

    removeNode(root, 18);

    printTree(root);
    cout << endl;

    return 1;
}
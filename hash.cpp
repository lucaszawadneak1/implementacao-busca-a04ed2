#include <iostream>

using namespace std;

// operação de busca tabela hash com encadeamento interno

struct Node
{
    int key;
    Node *next;
};

Node *search(Node *head, int key)
{
    Node *x = head;
    while (x != NULL && x->key != key)
    {
        x = x->next;
    }
    return x;
}

// função hash para strings
int hash(string s)
{
    int h = 0;
    for (int i = 0; i < s.length(); i++)
    {
        h = (h * 256 + s[i]) % 1000000007;
    }
    return h;
}

// operação de inserção tabela hash com encadeamento interno
void insert(Node *T, string value)
{

    int i = hash(value);
    Node *x = new Node;
    x->key = value;
    x->next = T[i];
    T[i] = x;
}

// operação de remoção tabela hash
void remove(Node *T, int key)
{
    int i = hash(key);
    Node *x = T;
    Node *y = NULL;
    while (x != NULL && x->key != key)
    {
        y = x;
        x = x->next;
    }
    if (x != NULL)
    {
        if (y == NULL)
        {
            T = x->next;
        }
        else
        {
            y->next = x->next;
        }
        delete x;
    }
}

// build node
Node *buildNode(int lenght)
{
    Node *x = new Node;
    for (int i = 0; i < lenght; i++)
    {
        x->key = i;
        x->next = NULL;
    }

    return x;
}

int main()
{

    // teste da função hash
    cout << hash("abc") << endl;
    cout << hash("cba") << endl;
    cout << hash("bca") << endl;

    // teste da tabela hash
    Node *head = buildNode(100);

    for (int i = 0; i < 100; i++)
    {
        T[i] = NULL;
    }
    insert(head, "abc");
    insert(head, "cba");
    insert(head, "bca");

    cout << search(head, "abc") << endl;
    cout << search(head, "cba") << endl;
    cout << search(head, "bca") << endl;
}
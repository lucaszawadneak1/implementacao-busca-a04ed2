#include <iostream>

using namespace std;

// Define a struct para representar um nó da árvore
struct Node
{
    int data;
    Node *left;
    Node *right;
};

// Função para verificar se duas árvores são espelhos uma da outra
bool eh_espelho(Node *root1, Node *root2)
{
    // Se ambas as árvores forem nulas, elas são espelhos uma da outra
    if (!root1 && !root2)
        return true;
    // Se apenas uma das árvores é nula, elas não são espelhos uma da outra
    if (!root1 || !root2)
        return false;
    // Se os dados dos nós raízes das árvores não forem iguais, elas não são espelhos uma da outra
    if (root1->data != root2->data)
        return false;
    // Verifique se o filho esquerdo da primeira árvore é um espelho do filho direito da segunda árvore
    // e o filho direito da primeira árvore é um espelho do filho esquerdo da segunda árvore
    return eh_espelho(root1->left, root2->right) && eh_espelho(root1->right, root2->left);
}

// Função que cria uma árvore espelho de uma árvore dada
Node *cria_espelho(Node *root)
{
    Node *espelho = new Node;

    if (root == NULL)
        return root;
    if (root->left == NULL && root->right == NULL)
        return root;

    espelho->data = root->data;
    espelho->left = cria_espelho(root->right);
    espelho->right = cria_espelho(root->left);
    return espelho;
}

int main()
{
    // Crie alguns nós da árvore
    Node *root1 = new Node{1};
    root1->left = new Node{2};
    root1->right = new Node{3};
    root1->left->left = new Node{4};
    root1->left->right = new Node{5};

    Node *root2 = new Node{1};
    root2->left = new Node{3};
    root2->right = new Node{2};
    root2->right->left = new Node{123};
    root2->right->right = new Node{442};

    cout << "Não devem ser espelho: \n";

    // Verifique se as árvores são espelhos uma da outra
    if (eh_espelho(root1, root2))
    {
        cout << "As árvores são espelhos uma da outra." << endl;
    }
    else
    {
        cout << "As árvores não são espelhos uma da outra." << endl;
    }

    cout << "Devem ser espelho\n";

    Node *espelho = cria_espelho(root1);

    // Verifique se as árvores são espelhos uma da outra
    if (eh_espelho(root1, espelho))
    {
        cout << "As árvores são espelhos uma da outra." << endl;
    }
    else
    {
        cout << "As árvores não são espelhos uma da outra." << endl;
    }

    return 0;
}

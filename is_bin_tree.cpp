#include <iostream>

using namespace std;

struct Node
{
    int data;
    Node *left;
    Node *right;
};

bool is_bin_tree(Node *root)
{
    if (root == NULL)
        return true;

    if (root->left != NULL && root->left->data > root->data)
        return false;

    if (root->right != NULL && root->right->data < root->data)
        return false;

    return is_bin_tree(root->left) && is_bin_tree(root->right);
}

int main()
{
    Node *not_binary = new Node{1};
    not_binary->left = new Node{2};
    not_binary->right = new Node{3};
    not_binary->left->left = new Node{4};
    not_binary->left->right = new Node{5};

    Node *bin_tree = new Node{5};
    bin_tree->left = new Node{3};
    bin_tree->right = new Node{7};
    bin_tree->left->left = new Node{2};
    bin_tree->left->right = new Node{4};
    bin_tree->right->left = new Node{6};
    bin_tree->right->right = new Node{8};

    cout << is_bin_tree(not_binary) << endl;
    cout << is_bin_tree(bin_tree) << endl;
    return 1;
}
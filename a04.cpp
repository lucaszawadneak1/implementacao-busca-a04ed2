#include <iostream>

using namespace std;

struct t_time
{
    int hour;
    int minute;
    int second;
};

int time_cmp(t_time t1, t_time t2)
{
    if (t1.hour > t2.hour)
        return 1;
    else if (t1.hour < t2.hour)
        return -1;
    else
    {
        if (t1.minute > t2.minute)
            return 1;
        else if (t1.minute < t2.minute)
            return -1;
        else
            return 0;
    }
}

struct t_timetable
{
    t_time time;
    string *value;
    t_timetable *next;
};

void put(t_time key, string value, t_timetable *table)
{
    t_timetable *p = new t_timetable;
    p->time = key;
    p->value = new string(value);

    if (time_cmp(p->time, table->time) == 1)
    {
        p->next = table->next;
        table->next = p;
    }
    else
    {
        p->next = table;
        table = p;
    }
}

string get(t_time key, t_timetable *table)
{
    t_timetable *p = table;
    while (p != NULL)
    {
        if (time_cmp(p->time, key) == 0)
            return *p->value;
        p = p->next;
    }
    return "Not found";
}

void del(t_time key, t_timetable *table)
{
    t_timetable *p = table;
    while (p->next != NULL)
    {
        if (time_cmp(p->next->time, key) == 0)
        {
            t_timetable *q = p->next;
            p->next = q->next;
            delete q;
            return;
        }
        p = p->next;
    }
}

bool contains(t_time key, t_timetable *table)
{
    t_timetable *p = table;
    while (p != NULL)
    {
        if (time_cmp(p->time, key) == 0)
            return true;
        p = p->next;
    }
    return false;
}

bool get_empty(t_timetable *table)
{
    return table != NULL;
}

int size(t_timetable *table)
{
    int count = 0;
    t_timetable *p = table;

    while (p != NULL)
    {
        count++;
        p = p->next;
    }
    return count;
}

t_time min(t_timetable *table)
{
    return table->time;
}

t_time max(t_timetable *table)
{
    t_timetable *p = table;
    while (p->next != NULL)
        p = p->next;
    return p->time;
}

t_time floor(t_time key, t_timetable *table)
{
    t_timetable *p = table;
    while (p->next != NULL)
    {
        if (time_cmp(p->next->time, key) == 1)
            return p->time;
        p = p->next;
    }
    return p->time;
}

t_time ceiling(t_time key, t_timetable *table)
{
    t_timetable *p = table;
    while (p->next != NULL)
    {
        if (time_cmp(p->next->time, key) == 1)
            return p->next->time;
        p = p->next;
    }
    return p->time;
}

int get_rank(t_time key, t_timetable *table)
{
    int count = 0;
    t_timetable *p = table;
    while (p != NULL)
    {
        if (time_cmp(p->time, key) == 0)
            return count;
        count++;
        p = p->next;
    }
    return -1;
}

t_time select(int k, t_timetable *table)
{
    t_timetable *p = table;
    for (int i = 0; i < k; i++)
        p = p->next;
    return p->time;
}

void delete_min(t_timetable *table)
{
    t_timetable *p = table;
    table = table->next;
    delete p;
}

void delete_max(t_timetable *table)
{
    t_timetable *p = table;
    while (p->next->next != NULL)
        p = p->next;
    delete p->next;
    p->next = NULL;
}

int size_range(t_time lo, t_time hi, t_timetable *table)
{
    int count = 0;
    t_timetable *p = table;
    while (p != NULL)
    {
        if (time_cmp(p->time, lo) == 1 && time_cmp(p->time, hi) == -1)
            count++;
        p = p->next;
    }
    return count;
}

int main()
{
    t_timetable *table = new t_timetable;
    table->next = NULL;
    table->time.hour = 0;
    table->time.minute = 0;
    table->time.second = 0;

    t_time key;
    key.hour = 1;
    key.minute = 1;
    key.second = 1;
    put(key, "Hello", table);

    key.hour = 2;
    key.minute = 2;
    key.second = 2;
    put(key, "World", table);

    key.hour = 3;
    key.minute = 3;
    key.second = 3;
    put(key, "!", table);

    key.hour = 4;
    key.minute = 4;
    key.second = 4;
    put(key, "!", table);

    bool is_empty = get_empty(table);
    int key_rank = get_rank(key, table);

    cout << "size: " << size(table) << endl;
    cout << "get: " << get(key, table) << endl;
    cout << "contains: " << contains(key, table) << endl;
    cout << "empty: " << empty << endl;
    cout << "min: " << min(table).hour << ":" << min(table).minute << ":" << min(table).second << endl;
    cout << "max: " << max(table).hour << ":" << max(table).minute << ":" << max(table).second << endl;
    cout << "floor: " << floor(key, table).hour << ":" << floor(key, table).minute << ":" << floor(key, table).second << endl;
    cout << "ceiling: " << ceiling(key, table).hour << ":" << ceiling(key, table).minute << ":" << ceiling(key, table).second << endl;
    cout << "rank: " << key_rank << endl;
    cout << "select: " << select(2, table).hour << ":" << select(2, table).minute << ":" << select(2, table).second << endl;
    cout << "size_range: " << size_range(key, key, table) << endl;

    delete_min(table);
    delete_max(table);
    del(key, table);

    cout << "size: " << size(table) << endl;
    cout << "get: " << get(key, table) << endl;
    cout << "contains: " << contains(key, table) << endl;

    return 0;
}